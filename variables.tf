variable "timeweb_token" {
  description = "TimeWebCloud Token"
  type        = string
  nullable    = true
  default     = null
}

variable "name" {
  description = "Name for k3s cluster"
  type        = string
  default     = null
}

variable "ssh_private_key" {
  description = "Local paths of SSH private key"
  type        = string
  default     = null
}

variable "kubeconfig_path" {
  description = "Local path for kubeconfig"
  type        = string
  nullable    = true
  default     = null
}

variable "master" {
  description = "Settings for master servers"
  type = object({
    project_name  = optional(string)
    location      = optional(string)
    cpu_frequency = optional(number)
    disk_type     = optional(string)
    preset = optional(object({
      cpu  = number
      ram  = number
      disk = number

      price = optional(object({
        min = number
        max = number
      }))
    }))

    os = object({
      name    = string
      version = string
    })

    ssh_keys = optional(list(string))
    ssh_keys_paths = optional(list(object({
      name = string
      path = string
    })))

    configurator = optional(object({
      cpu  = number
      ram  = number
      disk = number
    }))
  })

  default = null
}

variable "workers" {
  description = "Settings for workers servers"
  type = map(object({
    project_name  = optional(string)
    location      = optional(string)
    cpu_frequency = optional(number)
    disk_type     = optional(string)
    preset = optional(object({
      cpu  = number
      ram  = number
      disk = number

      price = optional(object({
        min = number
        max = number
      }))
    }))

    os = object({
      name    = string
      version = string
    })

    ssh_keys = optional(list(string))
    ssh_keys_paths = optional(list(object({
      name = string
      path = string
    })))

    configurator = optional(object({
      cpu  = number
      ram  = number
      disk = number
    }))
  }))

  default = null
}