locals {
  kubeconfig_path = var.kubeconfig_path != null ? var.kubeconfig_path : "./${var.name}.kubeconfig"
  master_ip       = module.master.server_public_ip
}

resource "null_resource" "master" {
  connection {
    type        = "ssh"
    user        = "root"
    private_key = file(var.ssh_private_key)
    host        = local.master_ip
  }

  provisioner "remote-exec" {
    inline = [
      "echo \"Setting up K3S master\""
    ]
  }

  provisioner "local-exec" {
    command = join(" ", [
      "k3sup install",
      "--ip ${local.master_ip}",
      "--context ${var.name}",
      "--ssh-key ${var.ssh_private_key}",
      "--user root",
      "--local-path ${local.kubeconfig_path}",
    ])
  }
}

resource "null_resource" "worker" {
  for_each = module.workers
  connection {
    type        = "ssh"
    user        = "root"
    private_key = file(var.ssh_private_key)
    host        = each.value.server_public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "echo \"Setting up K3S worker\""
    ]
  }

  provisioner "local-exec" {
    command = join(" ", [
      "k3sup join",
      "--ip ${each.value.server_public_ip}",
      "--server-ip ${local.master_ip}",
      "--ssh-key ${var.ssh_private_key}",
      "--user root"
    ])
  }

  depends_on = [null_resource.master]
}